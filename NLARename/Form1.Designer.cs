﻿namespace NLARename
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.listNLAs = new System.Windows.Forms.ListBox();
            this.newName = new System.Windows.Forms.TextBox();
            this.btnRename = new System.Windows.Forms.Button();
            this.lblNewName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listNLAs
            // 
            this.listNLAs.FormattingEnabled = true;
            this.listNLAs.Items.AddRange(new object[] {
            "Netzwerk",
            "Netzwerk  2",
            "Netzwerk  3"});
            this.listNLAs.Location = new System.Drawing.Point(10, 15);
            this.listNLAs.Name = "listNLAs";
            this.listNLAs.Size = new System.Drawing.Size(270, 160);
            this.listNLAs.TabIndex = 0;
            this.listNLAs.SelectedIndexChanged += new System.EventHandler(this.listNLAs_SelectedIndexChanged);
            // 
            // newName
            // 
            this.newName.Location = new System.Drawing.Point(72, 180);
            this.newName.MaxLength = 128;
            this.newName.Name = "newName";
            this.newName.Size = new System.Drawing.Size(208, 20);
            this.newName.TabIndex = 1;
            // 
            // btnRename
            // 
            this.btnRename.Location = new System.Drawing.Point(90, 224);
            this.btnRename.Name = "btnRename";
            this.btnRename.Size = new System.Drawing.Size(104, 24);
            this.btnRename.TabIndex = 2;
            this.btnRename.Text = "Rename";
            this.btnRename.UseVisualStyleBackColor = true;
            this.btnRename.Click += new System.EventHandler(this.btnRename_Click);
            // 
            // lblNewName
            // 
            this.lblNewName.AutoSize = true;
            this.lblNewName.Location = new System.Drawing.Point(7, 187);
            this.lblNewName.Name = "lblNewName";
            this.lblNewName.Size = new System.Drawing.Size(59, 13);
            this.lblNewName.TabIndex = 3;
            this.lblNewName.Text = "Rename to";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.lblNewName);
            this.Controls.Add(this.btnRename);
            this.Controls.Add(this.newName);
            this.Controls.Add(this.listNLAs);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "NLA Rename";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listNLAs;
        private System.Windows.Forms.TextBox newName;
        private System.Windows.Forms.Button btnRename;
        private System.Windows.Forms.Label lblNewName;
    }
}

