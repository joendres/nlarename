﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace NLARename
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void doRename()
        {
            MessageBox.Show(listNLAs.SelectedItem + newName.Text);
        }

        private void btnRename_Click(object sender, EventArgs e)
        {
            // Hier besser die ganze Fehlerprüfung hin, erlaubte Zeichen usw.
            if ("" == newName.Text)
                MessageBox.Show("You must enter a name.", "Name Entry Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            else
                if (System.Windows.Forms.DialogResult.OK == MessageBox.Show(
                    "Rename network location '" + 
                    (string)listNLAs.SelectedItem +
                    "' to '" + newName.Text + "'?", "Rename Network Location",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation))
                    doRename();
        }

        private void listNLAs_SelectedIndexChanged(object sender, EventArgs e)
        {
            newName.Text = (string)listNLAs.SelectedItem;
        }

    }
}
